FROM python:3-windowsservercore

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop';"]

ADD "https://github.com/Microsoft/iis-docker/blob/master/windowsservercore/ServiceMonitor.exe?raw=true" /ServiceMonitor.exe
COPY Install-Gitlab-CI-Runner.ps1 Install-Gitlab-CI-Runner.ps1
COPY Register-Gitlab-CI-Runner.ps1 Register-Gitlab-CI-Runner.ps1

RUN .\Install-Gitlab-CI-Runner.ps1

# Install git
RUN iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
RUN choco install git -y -params '"/GitAndUnixToolsOnPath"'


ENTRYPOINT ["C:\\ServiceMonitor.exe", "gitlab-runner"]
