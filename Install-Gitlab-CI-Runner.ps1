# Install Gitlab CI Runner

$gitlab_ci_multi_runner = "$env:windir\System32\gitlab-ci-multi-runner.exe"

if (-not (Test-Path $gitlab_ci_multi_runner)) {
    $wc = New-Object net.webclient
    $wc.Downloadfile("https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-windows-amd64.exe", $gitlab_ci_multi_runner)
}

# Stop-Service 'gitlab-runner'
# &$gitlab_ci_multi_runner uninstall

&$gitlab_ci_multi_runner install -user .\LocalSystem  -c "C:\gitlab_config.toml"
Start-Service 'gitlab-runner'

# Allow interactive access to gitlab runner
$params = @{
  "Namespace" = "root\CIMV2"
  "Class" = "Win32_Service"
  "Filter" = "DisplayName='gitlab-runner'"
}
$service = Get-WmiObject @params
# https://msdn.microsoft.com/en-us/library/aa384901.aspx?f=255&MSPPError=-2147217396
$service.Change(
  $null,
  $null,
  $null,
  $null,
  $null,
  1,
  $null,
  $null,
  $null,
  $null,
  $null)
