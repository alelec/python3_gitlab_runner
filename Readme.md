# Windows docker image python3 with gitlab_runner
```
docker pull registry.gitlab.com/alelec/python3_gitlab_runner

$REGISTRATION_TOKEN="<token from https://gitlab.com/admin/runners or project ci settings>"
$IMAGE="registry.gitlab.com/alelec/python3_gitlab_runner"
$NAME="windows_python3_gitlab_runner"
$DESCRIPTION="windows_python3_gitlab_runner"
$SERVER="https://gitlab.com"
  
docker pull $IMAGE
docker run -d --name "gitlab-$NAME" --restart always $IMAGE
 
docker exec "gitlab-$NAME" `
 gitlab-ci-multi-runner register --non-interactive `
  -c "C:\gitlab_config.toml" `
  --url=$SERVER `
  --registration-token="$REGISTRATION_TOKEN" `
  --description="$DESCRIPTION" `
  --executor=shell `
  --shell=powershell `
  --tag-list=$NAME
```