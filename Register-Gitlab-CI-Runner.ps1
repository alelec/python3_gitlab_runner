# Register Gitlab CI Runner
[CmdletBinding(DefaultParameterSetName="Standard")]
param(
    [string]
    [ValidateNotNullOrEmpty()]
    $registration_token = "",
    [string]
    [ValidateNotNullOrEmpty()]
    $url = "https://gitlab.com",
    [string]
    [ValidateNotNullOrEmpty()]
    $executor,
    [string]
    [ValidateNotNullOrEmpty()]
    $description = "WindowsRunner",
    [string]
    [ValidateNotNullOrEmpty()]
    $tags = "windows"
)
$gitlab_ci_multi_runner = "$env:windir\System32\gitlab-ci-multi-runner.exe"

# Setup docker specific settings
if ($executor -eq "docker") {
    $docker_host = "127.0.0.1:2375"
    $docker_image = "microsoft/windowsservercore"
    # Ensure docker is listening on a local port as well as the pipe
    $config = @"
{
    "hosts": [
              "npipe://",
              "tcp://127.0.0.1:2375"
              ]
}
"@
    $config | Out-File -encoding ascii "c:\programdata\docker\config\daemon.json"
    Restart-Service 'docker'
} # end of docker specific config

if ($executor -eq "docker") {
&$gitlab_ci_multi_runner register -c "C:\gitlab_config.toml" --non-interactive --url=$url --registration-token=$registration_token --description=$description --executor=$executor --docker-host=$docker_host --docker-image=$docker_image --tag-list=$tags --tls-ca-file=$cert
}

if ($executor -eq "shell") {
# Shell executor required currently to build docker images with, until I get a docker-in-docker image on windows.
$shell = "powershell"
&$gitlab_ci_multi_runner register -c "C:\gitlab_config.toml" --non-interactive --url=$url --registration-token=$registration_token --description=$description --executor=$executor --shell=$shell --tag-list=$tags --tls-ca-file=$cert
}
